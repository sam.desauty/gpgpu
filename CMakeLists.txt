cmake_minimum_required(VERSION 3.10)
project(gpgpu CXX)

set(CMAKE_CXX_STANDARD 17)

include(CheckLanguage)
check_language(CUDA)
if (CMAKE_CUDA_COMPILER)
    enable_language(CUDA)
    find_package(CUDA 10.2 REQUIRED)
endif()

if(NOT DEFINED CMAKE_CUDA_STANDARD)
    set(CMAKE_CUDA_STANDARD 14)
    set(CMAKE_CUDA_STANDARD_REQUIRED ON)
endif()

include(FetchContent)

FetchContent_Declare(
        CLI11
        URL https://github.com/CLIUtils/CLI11/archive/v1.8.0.tar.gz
)
FetchContent_MakeAvailable(CLI11)

set(EXECUTABLE_NAME main)
set(GPU_EXECUTABLE_NAME gpu_main)

set(CMAKE_CXX_FLAGS_RELEASE  "-O3 -Wall -Wextra -Werror -pedantic")
set(CMAKE_CXX_FLAGS_DEBUG    "-g  -Wall -Wextra -Werror -pedantic")
set(CMAKE_CUDA_FLAGS_DEBUG "-lineinfo")


add_subdirectory(src)

