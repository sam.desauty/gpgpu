### Implementation of graph cuts with Cuda

Graph cut algo : Goldberg-Tarjan (push relabel)
considers one node at a time, local scope, inherently parallel

### Run the graph cut with different options

To be reminded of the different options, use --help

#### Options

-fi image.ppm: file input
-di directory: directory input 
-o <output_file.ppm/output_folder> : specify the output name, by default image_o.ppm
-m <CPU/GPU/GPU2> : specify the version of the code you want to run, GPU is the naive gpu implementation and GPU2 is the one using the stencil pattern

ex :
./gpu_main -di ../data/inputs/ -o out -m GPU
./gpu_main -fi ../data/inputs/plane.ppm -o plane_out.ppm -m GPU
