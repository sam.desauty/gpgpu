#include "color.hh"


Color& Color::operator+=(const Color &rhs) {
    r += rhs.r;
    g += rhs.g;
    b += rhs.b;

    return *this;
}

Color& Color::operator-=(const Color &rhs) {
    r -= rhs.r;
    g -= rhs.g;
    b -= rhs.b;

    return *this;
}

int Color::L1_norm() const {
    return std::abs(r) + std::abs(g) + std::abs(b);
}

int Color::L2_norm() const {
    return std::sqrt(r * r + g * g + b * b);
}

Color Color::transfer(size_t num_bins) const {
    return Color(r/num_bins, g/num_bins, b/num_bins);
}


Color operator+(Color lhs, const Color& rhs) {
    lhs += rhs;
    return lhs;
}

Color operator-(Color lhs, const Color& rhs) {
    lhs -= rhs;
    return lhs;
}

bool Color::operator==(const Color &rhs) const {
    return r == rhs.r && g == rhs.g && b == rhs.b;
}

bool Color::operator!=(const Color &rhs) const {
    return !(*this == rhs);
}

bool Color::operator<(const Color &rhs) const {
    return r < rhs.r && g < rhs.g && b < rhs.b;
}

std::ostream& operator<<(std::ostream &out, Color c)
{
    out << (unsigned)c.r << ' ';
    out << (unsigned)c.g << ' ';
    out << (unsigned)c.b << ' ';
    return out;
}