//
// Created by mattrouss on 5/23/20.
//

#pragma once

#include <vector>
#include <iostream>

template <typename T>
class Table {
public:
    using row_t = std::vector<T>;
    using grid_t = std::vector<row_t>;

    using iterator = typename grid_t::iterator;
    using const_iterator = typename grid_t::const_iterator;

    Table(): grid_(), width_(), height_() {}

    Table(size_t width, size_t height): width_(width), height_(height) {
        grid_.resize(height_);
        for (auto& row: grid_)
            row.resize(width_);
    }

    row_t& operator[](size_t idx) {
        return grid_[idx];
    }

    row_t operator[](size_t idx) const {
        return grid_[idx];
    }

    iterator begin() { return grid_.begin(); }

    iterator end() { return grid_.end(); }

    const_iterator begin() const { return grid_.begin(); }

    const_iterator end() const { return grid_.end(); }

    Table& operator+=(const Table& rhs) {
        for (auto i = 0u; i < height_; ++i)
            for (auto j = 0u; j < width_; ++j)
                grid_[i][j] += rhs[i][j];

        return *this;
    }

    [[nodiscard]] size_t width() const {
        return width_;
    }

    [[nodiscard]] size_t height() const {
        return height_;
    }

    T max() const {
        std::vector<T> row_max;
        std::transform(grid_.begin(), grid_.end(), std::back_inserter(row_max),
                [](row_t row) -> T {
            return *std::max_element(row.begin(), row.end());
        });

        return *std::max_element(row_max.cbegin(), row_max.cend());
    }

    friend std::ostream& operator<<(std::ostream& os, const Table<T>& t) {
        os << "Table {" << std::endl;
        for (auto& row: t.grid_) {
            for (auto& elm: row) {
                os << elm << " ";
            }
            os << std::endl;
        }
        os << "}";

    return os;
    }

private:
    grid_t grid_;
    size_t width_, height_;
};

template <typename T>
inline Table<T> operator+(Table<T> lhs, const Table<T>& rhs) {
    lhs += rhs;
    return lhs;
}
