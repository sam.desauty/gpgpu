#include "image.hh"


void write_mask(char* mask, size_t height, size_t width, const std::string& filename) {
    std::ofstream file(filename);
    file << "P3" << '\n';
    file << width << ' ' << height << '\n';
    file << 255 << '\n';
    for(size_t i = 0; i < height * width; i++)
    {
        if (mask[i] == 1)
            file << "255 255 255 ";
        else
            file << "0 0 0 ";
        file << '\n';
    }
}

Image::Image(const std::string& img, const std::string& m, size_t n_bins)
: hist_foreground(Histogram(n_bins)), hist_background(Histogram(n_bins))
{
    read_image(img);
    read_mask(m);
    compute_histogram();
}

Image::~Image() {
    delete[] _image;
    delete[] _mask;
}

void Image::read_image(const std::string& img)
{
    std::ifstream image(img);
    if (!image)
        throw std::runtime_error("Error opening file: " + img);
    std::string w1;
    std::string w2;
    std::string w3;
    image >> w1; //P3
    image >> w1; // w
    while (w1[0] == '#') {
        std::getline(image, w1); // Read commented line
        image >> w1; // Read w
    }
    _width = size_t(std::stoi(w1));
    image >> w1; // h
    _height = size_t(std::stoi(w1));
    image >> w1; // 255
    _image = new Color[_height * _width];
    for (size_t i = 0; i < _height * _width; i++)
    {
            image >> w1; image >> w2;  image >> w3;
            Color c(std::stoi(w1), std::stoi(w2), std::stoi(w3));
            _image[i] = c;
    }
}

void Image::read_mask(const std::string& m)
{
    std::ifstream mask(m);
    if (!mask)
        throw std::runtime_error("Error opening file: " + m);
    std::string w1;
    std::string w2;
    std::string w3;
    mask >> w1; //P3
    mask >> w1; // w
    while (w1[0] == '#') {
        std::getline(mask, w1); // Read commented line
        mask >> w1; // Read w
    }
    _width = size_t(std::stoi(w1));
    mask >> w1; // h
    _height = size_t(std::stoi(w1)); 
    mask >> w1; // 255
   _mask = new char[_height * _width];
    for (size_t i = 0; i < _height * _width; i++)
    {
        mask >> w1; mask >> w2; mask >> w3;
        if (w1 == w2 && w2 == w3 && w1 == "255")
            _mask[i] = 1; // foreground
        else if (w1 == w2 && w2 == w3 && w1 == "0")
            _mask[i] = -1; // not annotated
        else
            _mask[i] = 0; // background
    }
}


void Image::masked_image(const std::string& filename)
{
    write_mask(_mask, _height, _width, filename);
}

void Image::mask_writer(const std::string& filename)
{
    std::ofstream file(filename);
    file << "P3" << '\n';
    file << _width << ' ' << _height << '\n';
    file << 255 << '\n';
    for(size_t i = 0; i < _height * _width; i++)
    {
        if (_mask[i] == 1)
            file << "255 255 255 ";
        else
            file << "0 0 0 ";
        file << '\n';
    }
}

size_t Image::width() const {
    return _width;
}

size_t Image::height() const {
    return _height;
}

void Image::compute_histogram(){
    Color color;
    for (size_t i = 0 ; i < _width * _height; ++i)
    {
        color = _image[i];
        if (_mask[i] == 1) //foreground
            hist_foreground.add_color(color);
        if (_mask[i] == 0) //background
            hist_background.add_color(color);
    }
}

char* Image::get_image_array() const {
    return reinterpret_cast<char*>(_image);
}

char* Image::get_mask_array() const {
    return _mask;
}

Color* Image::get_color_array() const {
    return _image;
}


Color& Image::get_color(size_t i, size_t j) const {
    return _image[i * _width + j];
}

char& Image::get_mask(size_t i, size_t j) const {
    return _mask[i * _width + j];
}

