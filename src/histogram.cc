//
// Created by mattrouss on 5/26/20.
//

constexpr float EPSILON = 10e-3;

#include "histogram.hh"


size_t Histogram::color_idx(const Color& c) const {
    auto bin_color = c.transfer(pixel_groups_);
    return bin_color.r * n_bins_ * n_bins_ + bin_color.g * n_bins_ + bin_color.b;
}

size_t Histogram::color_idx(size_t i, size_t j, size_t k) const {
    return i * n_bins_ * n_bins_ + j * n_bins_ + k;
}

void Histogram::add_color(const Color &c) {
    //quantization of the color spectrum
    occ_[color_idx(c)]++;
    hist_size_++;
}

size_t Histogram::get_occ(const Color &c) const {
    return occ_[color_idx(c)];
}

std::ostream& operator<<(std::ostream &out, const Histogram& h) {
    size_t occ;
    for (auto i = 0u; i < h.n_bins_; ++i) {
        for (auto j = 0u; j < h.n_bins_; ++j) {
            for (auto k = 0u; k < h.n_bins_; ++k) {
                occ = h.occ_[h.color_idx(i, j, k)];
                if (occ) {
                    std::cout << "(" << i << ", " << j << ", " << k << ") : ";
                    for(auto l = 0u; l < occ; ++l)
                        std::cout << "\u25A0";
                    std::cout << "   " << occ << std::endl;
                }
            }
        }
    }
    return out;
}

float Histogram::get_regional_penalty(const Color &c) const {
    size_t occ = get_occ(c);
    return -logf(((float)occ / (float)hist_size_) + EPSILON);
}

