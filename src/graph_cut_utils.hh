//
// Created by mattrouss on 6/12/20.
//

#pragma once

#include <cstddef>

#include "image.hh"

constexpr size_t SIGMA = 1800;
constexpr size_t LAMBDA = 60;
constexpr size_t HEIGHT_MAX = 1;

bool is_border(int i, int length);

int likelihood(const Color& a, const Color& b);


