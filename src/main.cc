#include "image.hh"
#include "graph.hh"

#include <chrono>


int main(int argc, char* argv[])
{
    if (argc < 3){
		std::cout << "args are image and mask: ./main image.ppm mask.ppm\n";
		return 1;
	}

    Image img(argv[1], argv[2], 15);
    Graph g(img);

    auto t0 = std::chrono::high_resolution_clock::now();
    g.graph_cut(img);
    auto t1 = std::chrono::high_resolution_clock::now();

    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0);
    std::cout << "Graph cut exectution time: " << duration.count() << "ms" << std::endl;

    img.masked_image("output.ppm");
    return 0;
}
