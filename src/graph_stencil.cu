//
// Created by mattrouss on 5/28/20.
//

#include "graph_stencil.hh"

    [[gnu::noinline]]
void _abortError_stencil(const char* msg, const char* fname, int line)
{
    cudaError_t err = cudaGetLastError();
    std::cerr << msg << " (" << fname << ", line: " << line << ")" << std::endl;
    std::cerr << "Error " << cudaGetErrorName(err) << ": " << cudaGetErrorString(err) << std::endl;
    std::exit(1);
}

#define _abortError_stencil(msg) _abortError_stencil(msg, __FUNCTION__, __LINE__)

namespace graph_cut {
    template<typename T>
        T max(T* arr, size_t size) {
            T max = arr[0];
            for (size_t i = 0; i < size; i++) {
                if (arr[i] > max)
                    max = arr[i];
            }
            return max;
        }
}

__global__ void any_active_stencil(DeviceTable excess_flows, DeviceTable heights,
        int width, int height, size_t height_max, int* active) {

    int x = blockIdx.x * blockDim.x + threadIdx.x;
    int y = blockIdx.y * blockDim.y + threadIdx.y;
    if (x >=  width-1 || y >= height-1|| x == 0 || y == 0 )
        return;

    // Initialize local sum to avoid sequential execution
    __shared__ int local_active;
    if (threadIdx.x == 0 && threadIdx.y == 0) local_active = 0;
    __syncthreads();


    int* excess_flows_line = (int*)(excess_flows.data + y * excess_flows.pitch);
    size_t* heights_line = (size_t*)(heights.data + y * heights.pitch);
    if (excess_flows_line[x] > 0 && heights_line[x] < height_max)
        atomicAdd(&local_active, 1);

    __syncthreads();
    if (threadIdx.x == 0 && threadIdx.y == 0) {
        atomicAdd(active, local_active);
    }

}

__device__ void neighbours_stencil(Neigh* n, int i, int j, int width, int height)
{
    if (i+1 != height)
        n[0] = {i+1, j, 1, 0}; //bottom
    else
        n[0] = {0, 0, -1, -1};
    if (i-1 != -1)
        n[1] = {i-1, j, 0, 1}; //top
    else
        n[1] = {0, 0, -1, -1};
    if (j-1 != -1)
        n[2] = {i, j-1, 2, 3}; //left
    else
        n[2] = {0, 0, -1, -1};
    if (j+1 != width)
        n[3] = {i, j+1, 3, 2}; //right
    else
        n[3] = {0, 0, -1, -1};
}

__global__ void fill_capacities_stencil(DeviceTable image_colors, DeviceTable* capacities,
        size_t width, size_t height)
{
    int j = blockIdx.x * blockDim.x + threadIdx.x;
    int i = blockIdx.y * blockDim.y + threadIdx.y;
    if (i >= height -1 || j >= width-1 || i == 0 || j == 0)
        return;

    Color* ptr_line_color = (Color*)(image_colors.data + i * image_colors.pitch);

    Color* ptr_line_n_color;
    int* ptr_line_capacities;

    /***** top ****/

    ptr_line_capacities = (int *)(capacities[0].data + i * capacities[0].pitch);
    ptr_line_n_color = (Color*)(image_colors.data + (i-1) * image_colors.pitch);
    auto r = ptr_line_color[j].r - ptr_line_n_color[j].r;
    auto g = ptr_line_color[j].g - ptr_line_n_color[j].g;
    auto b = ptr_line_color[j].b - ptr_line_n_color[j].b;
    ptr_line_capacities[j] =  1/(0.1+abs(r) + abs(g) + abs(b));

    /***** bottom *****/

    ptr_line_capacities = (int *)(capacities[1].data + i * capacities[1].pitch);
    ptr_line_n_color = (Color*)(image_colors.data + (i+1) * image_colors.pitch);
    r = ptr_line_color[j].r - ptr_line_n_color[j].r;
    g = ptr_line_color[j].g - ptr_line_n_color[j].g;
    b = ptr_line_color[j].b - ptr_line_n_color[j].b;
    ptr_line_capacities[j] =  1/(0.1+abs(r) + abs(g) + abs(b));

    /***** left *****/

    ptr_line_capacities = (int *)(capacities[2].data + i * capacities[2].pitch);
    r = ptr_line_color[j].r - ptr_line_color[j-1].r;
    g = ptr_line_color[j].g - ptr_line_color[j-1].g;
    b = ptr_line_color[j].b - ptr_line_color[j-1].b;
    ptr_line_capacities[j] =  1/(0.1+abs(r) + abs(g) + abs(b));

    /***** right *****/

    ptr_line_capacities = (int *)(capacities[3].data + i * capacities[3].pitch);
    r = ptr_line_color[j].r - ptr_line_color[j+1].r;
    g = ptr_line_color[j].g - ptr_line_color[j+1].g;
    b = ptr_line_color[j].b - ptr_line_color[j+1].b;
    ptr_line_capacities[j] =  1/(0.1+abs(r) + abs(g) + abs(b));
}

#define TILE_WIDTH 34

__global__ void push_stencil(DeviceTable excess_flows, DeviceTable heights,
        DeviceTable* capacities,
        int width, int height, size_t height_max)
{
    int flow = 0;
    int j = blockIdx.x * blockDim.x + threadIdx.x;
    int i = blockIdx.y * blockDim.y + threadIdx.y;
    if (i >= height || j >= width)
        return;
    __shared__ size_t tile_heights[TILE_WIDTH][TILE_WIDTH];
    for (int idx = threadIdx.y; idx < TILE_WIDTH; idx += blockDim.y)
        for (int jdx = threadIdx.x; jdx < TILE_WIDTH; jdx += blockDim.x)
        {
            int x = blockDim.x * blockIdx.x + jdx - 1;
            int y = blockDim.y * blockIdx.y + idx - 1;
            if (y >= height || x >= width || x == -1|| y == -1)
                continue;
            tile_heights[idx][jdx] = heights.data[y * heights.pitch + x];
        }
    __syncthreads();

    if (i >= height - 1 || j >= width - 1 || i == 0 || j == 0)
        return;
    int x = threadIdx.y + 1;
    int y = threadIdx.x + 1;
    int* ptr_line_excess_flows = (int *)(excess_flows.data + i * excess_flows.pitch);
    if (ptr_line_excess_flows[j] <= 0 || tile_heights[x][y] >= height_max) //inactive
        return;
    /***** top ****/
    int* ptr_line_capacities = (int *)(capacities[0].data + i * capacities[0].pitch);
    int* ptr_line_n_capacities = (int *)(capacities[1].data + (i-1) * capacities[1].pitch); //opposite
    int* ptr_line_n_excess_flows = (int * )(excess_flows.data + (i-1) * excess_flows.pitch);
    if (tile_heights[x-1][y] == tile_heights[x][y] - 1)
    {
        //usage of atomic operations to ensure data integrity
        flow = min(ptr_line_capacities[j], ptr_line_excess_flows[j]);
        atomicSub(&ptr_line_excess_flows[j], flow);
        atomicAdd(&ptr_line_n_excess_flows[j], flow);
        atomicSub(&ptr_line_capacities[j], flow);
        atomicAdd(&ptr_line_n_capacities[j], flow);
    }
    /***** bottom *****/
    ptr_line_capacities = (int *)(capacities[1].data + i * capacities[1].pitch);
    ptr_line_n_capacities = (int *)(capacities[0].data + (i+1) * capacities[0].pitch); //opposite
    ptr_line_n_excess_flows = (int * )(excess_flows.data + (i+1) * excess_flows.pitch);
    if (tile_heights[x+1][y] == tile_heights[x][y] - 1)
    {
        flow = min(ptr_line_capacities[j], ptr_line_excess_flows[j]);
        atomicSub(&ptr_line_excess_flows[j], flow);
        atomicAdd(&ptr_line_n_excess_flows[j], flow);
        atomicSub(&ptr_line_capacities[j], flow);
        atomicAdd(&ptr_line_n_capacities[j], flow);
    }
    /***** left *****/
    ptr_line_capacities = (int *)(capacities[2].data + i * capacities[2].pitch);
    ptr_line_n_capacities = (int *)(capacities[3].data + i * capacities[3].pitch); //opposite
    if (tile_heights[x][y-1] == tile_heights[x][y] - 1)
    {
        flow = min(ptr_line_capacities[j], ptr_line_excess_flows[j]);
        atomicSub(&ptr_line_excess_flows[j], flow);
        atomicAdd(&ptr_line_excess_flows[j-1], flow);
        atomicSub(&ptr_line_capacities[j], flow);
        atomicAdd(&ptr_line_n_capacities[j-1], flow);
    }
    /***** right *****/
    ptr_line_capacities = (int *)(capacities[3].data + i * capacities[3].pitch);
    ptr_line_n_capacities = (int *)(capacities[2].data + i * capacities[2].pitch); //opposite
    if (tile_heights[x][y+1] == tile_heights[x][y] - 1)
    {
        flow = min(ptr_line_capacities[j], ptr_line_excess_flows[j]);
        atomicSub(&ptr_line_excess_flows[j], flow);
        atomicAdd(&ptr_line_excess_flows[j+1], flow);
        atomicSub(&ptr_line_capacities[j], flow);
        atomicAdd(&ptr_line_n_capacities[j+1], flow);
    }
}
__global__ void relabel_stencil(DeviceTable excess_flows, DeviceTable heights,
        DeviceTable* capacities,
        int width, int height, size_t height_max)
{
    int j = blockIdx.x * blockDim.x + threadIdx.x;
    int i = blockIdx.y * blockDim.y + threadIdx.y;
    if (i >= height || j >= width)
        return;

    __shared__ size_t tile_heights[TILE_WIDTH][TILE_WIDTH];

    for (int idx = threadIdx.y; idx < TILE_WIDTH; idx += blockDim.y)
        for (int jdx = threadIdx.x; jdx < TILE_WIDTH; jdx += blockDim.x)
        {
            int x = blockDim.x * blockIdx.x + jdx - 1;
            int y = blockDim.y * blockIdx.y + idx - 1;
            if (y >= height || x >= width || x == -1 || y == -1)
                continue;
            tile_heights[idx][jdx] = heights.data[y * heights.pitch + x];
        }

    __syncthreads();

    if (i >= height - 1 || j >= width - 1 || i == 0 || j == 0)
        return;

    int x = threadIdx.y + 1;
    int y = threadIdx.x + 1;

    int* ptr_line_excess_flows = (int * )(excess_flows.data + i * excess_flows.pitch);
    if (ptr_line_excess_flows[j] <= 0 || tile_heights[x][y] >= height_max) //inactive
        return;
    size_t my_height = height_max;
    /***** top ****/
    int* ptr_line_capacities = (int *)(capacities[0].data + i * capacities[0].pitch);
    if (ptr_line_capacities[j] > 0)
        my_height = min(my_height, tile_heights[x-1][y] + 1);
    /***** bottom *****/
    ptr_line_capacities = (int *)(capacities[1].data + i * capacities[1].pitch);
    if (ptr_line_capacities[j] > 0)
        my_height = min(my_height, tile_heights[x+1][y] + 1);
    /***** left *****/
    ptr_line_capacities = (int *)(capacities[2].data + i * capacities[2].pitch);
    if (ptr_line_capacities[j] > 0)
        my_height = min(my_height, tile_heights[x][y-1] + 1);
    /***** right *****/
    ptr_line_capacities = (int *)(capacities[3].data + i * capacities[3].pitch);
    if (ptr_line_capacities[j] > 0)
        my_height = min(my_height, tile_heights[x][y+1] + 1);

    size_t* ptr_line_heights = (size_t *)(heights.data + (i) * heights.pitch);
    ptr_line_heights[j] = my_height;
    tile_heights[x][y] = my_height;
}

#define STRUCT_ELT_SIZE 6

__global__ void erosion_dilatation_stencil(DeviceTable mask, DeviceTable new_mask, bool param, size_t height, size_t width)
{
    int j = blockIdx.x * blockDim.x + threadIdx.x;
    int i = blockIdx.y * blockDim.y + threadIdx.y;
    if (i >= height - STRUCT_ELT_SIZE || j >= width - STRUCT_ELT_SIZE)
        return;
    char* ptr_line_new_mask = (char*)(new_mask.data + i * new_mask.pitch);
    char* ptr_line_mask;
    for(int idx = i; idx < i + STRUCT_ELT_SIZE; idx++)
    {
        ptr_line_mask = (char*)(mask.data + idx * mask.pitch);
        for(int jdx = j; jdx < j + STRUCT_ELT_SIZE; jdx++)
        {
            if (param) // dilatation
            {
                if (ptr_line_mask[jdx] == 1)
                {
                    ptr_line_new_mask[j] = 1;
                    return;
                }
            }
            else // erosion
            {
                if (ptr_line_mask[jdx] != 1)
                {
                    ptr_line_new_mask[j] = 0;
                    return;
                }
            }
        }
    }
    if (param)
        ptr_line_new_mask[j] = 0;
    else
        ptr_line_new_mask[j] = 1;
}


__global__ void binarize_mask_stencil(DeviceTable mask, DeviceTable heights,
        size_t width, size_t height) {
    int j = blockIdx.x * blockDim.x + threadIdx.x;
    int i = blockIdx.y * blockDim.y + threadIdx.y;
    if (i >= height || j >= width)
        return;

    char* ptr_line_mask = (char*)(mask.data + i * mask.pitch);
    size_t* ptr_line_heights = (size_t*)(heights.data + i * heights.pitch);
    if (ptr_line_heights[j] > 0)
        ptr_line_mask[j] = 1;
    else
        ptr_line_mask[j] = 0;
}


Graph_Stencil::Graph_Stencil(const Image& img)
    : width_(img.width()), height_(img.height()), height_max_(HEIGHT_MAX)
{
    int block_size = 32;
    int w = std::ceil((float)width_ / block_size);
    int h = std::ceil((float)height_ / block_size);

    dimBlock_ = dim3(block_size, block_size);
    dimGrid_ = dim3(w, h);

    colors_ = DeviceTable(height_, width_, sizeof(Color));
    mask_ = DeviceTable(height_, width_, sizeof(char));

    auto* host_capacities = (DeviceTable*)std::malloc(4 * sizeof(DeviceTable));
    for (auto i = 0u; i < 4; ++i)
        host_capacities[i] = DeviceTable(height_, width_, sizeof(int));

    cudaError_t rc = cudaSuccess;
    rc = cudaMallocManaged(&capacities_, 4 * sizeof(DeviceTable));
    if (rc)
        _abortError_stencil("Error allocating array of capacity tables");

    rc = cudaMemcpy(capacities_, host_capacities, 4 * sizeof(DeviceTable), cudaMemcpyHostToDevice);
    if (rc)
        _abortError_stencil("Error copying array of capacity tables to device");

    excess_flows_ = DeviceTable(height_, width_, sizeof(int));
    heights_ = DeviceTable(height_, width_, sizeof(size_t));

    preflow_histogram_host(img);
}

Graph_Stencil::~Graph_Stencil() {
    cudaError_t rc = cudaSuccess;

    // Free capacity tables
    rc = cudaFree(capacities_);
    if (rc)
        _abortError_stencil("Error free array of capacity tables");
}

__device__ int L1_norm_stencil(Color a, Color b)
{
    a.r -= b.r;
    a.g -= b.g;
    a.b -= b.b;
    return std::abs(a.r) + std::abs(a.g) + std::abs(a.b);
}

__global__ void init_excess_flow_stencil(DeviceTable excess_flows, DeviceTable image_colors,
        Color mean_color_foreground, Color mean_color_background, int width,
        int height)
{
    int j = blockIdx.x * blockDim.x + threadIdx.x;
    int i = blockIdx.y * blockDim.y + threadIdx.y;
    if (i >= height-1 || j >= width-1 || i == 0 || j == 0)
        return;
    Color* ptr_line_colors = (Color*)(image_colors.data + i * image_colors.pitch);
    int* ptr_line_excess_flows = (int*)(excess_flows.data + i * excess_flows.pitch);

    auto f = L1_norm_stencil(ptr_line_colors[j], mean_color_foreground);
    auto b = L1_norm_stencil(ptr_line_colors[j], mean_color_background);
    ptr_line_excess_flows[j] = b - f;
}

Color Graph_Stencil::compute_mean(const Image &img, int seed) {
    Color mean_color = Color(0, 0, 0);
    float count = 0;
    for (size_t i = 0; i < height_; ++i)
    {
        for (size_t j = 0; j < width_; ++j)
        {
            if ((int)img.get_mask(i, j) == seed)
            {
                mean_color += img.get_color(i, j);
                count += 1;
            }
        }
    }
    return mean_color.transfer(count);
}

void Graph_Stencil::preflow_mean_host(const Image &img) {
    Color mean_color_foreground = compute_mean(img, 1);
    Color mean_color_background = compute_mean(img, 0);

    size_t* host_heights = (size_t*) std::calloc(width_ * height_, sizeof(size_t));
    int* host_capacities[4];
    int* host_excess_flows = (int*)std::malloc(width_ * height_ * sizeof(int));

    for (auto i = 0u; i < 4; ++i)
        host_capacities[i] = (int*)std::malloc(width_ * height_ * sizeof(int));

    for (size_t i = 0; i < height_; ++i)
    {
        for (size_t j = 0; j < width_; ++j)
        {
            vector<Neigh> neighbour = neighbours(i, j);
            for (auto n: neighbour)
                host_capacities[n.xy][i * width_ + j] = likelihood(img.get_color(i, j), img.get_color(n.i, n.j));

            auto f = (img.get_color(i, j)- mean_color_foreground).L1_norm();
            auto b = (img.get_color(i, j) - mean_color_background).L1_norm();
            host_excess_flows[i * width_ + j] = b - f;
        }
    }
    heights_.copy_from_host((char*)host_heights);
    excess_flows_.copy_from_host((char*)host_excess_flows);
    for (auto i = 0u; i < 4; ++i)
        capacities_[i].copy_from_host((char*)host_capacities[i]);
}

void Graph_Stencil::preflow_histogram_host(const Image &img) {
    size_t* host_heights = (size_t*) std::calloc(width_ * height_, sizeof(size_t));
    int* host_capacities[4];
    int* host_excess_flows = (int*)std::malloc(width_ * height_ * sizeof(int));

    for (auto i = 0u; i < 4; ++i)
        host_capacities[i] = (int*)std::malloc(width_ * height_ * sizeof(int));

    for (size_t i = 0; i < height_; ++i)
    {
        for (size_t j = 0; j < width_; ++j)
        {
            vector<Neigh> neighbour = neighbours(i, j);
            for (auto n: neighbour)
                host_capacities[n.xy][i * width_ + j] = likelihood(img.get_color(i, j), img.get_color(n.i, n.j));
        }
    }

    int K = 1 + HEIGHT_MAX;

    for (size_t i = 0; i < height_; ++i)
    {
        for (size_t j = 0; j < width_; ++j)
        {
            if (img.get_mask(i, j) == 1) //foreground
                host_excess_flows[i * width_ + j] = K;
            else if (img.get_mask(i, j) == 0) //background
                host_excess_flows[i * width_ + j] = - K;
            else {
                auto object_rp = img.hist_foreground.get_regional_penalty(img.get_color(i, j));
                auto background_rp = img.hist_background.get_regional_penalty(img.get_color(i, j));
                host_excess_flows[i * width_ + j] = LAMBDA * (background_rp - object_rp);
            }
        }
    }

    heights_.copy_from_host((char*)host_heights);
    excess_flows_.copy_from_host((char*)host_excess_flows);
    for (auto i = 0u; i < 4; ++i)
        capacities_[i].copy_from_host((char*)host_capacities[i]);

    std::free(host_excess_flows);
    std::free(host_heights);
    for (auto i = 0u; i < 4; ++i)
        std::free(host_capacities[i]);
}

void Graph_Stencil::preflow(const Image &img) {
    cudaError_t rc = cudaSuccess;
    Color mean_color_foreground = compute_mean(img, 1);
    Color mean_color_background = compute_mean(img, 0);

    colors_.copy_from_host((char*)img.get_color_array());

    fill_capacities_stencil<<<dimBlock_, dimGrid_>>>(colors_, capacities_, width_, height_);
    rc = cudaDeviceSynchronize();
    if (rc)
        _abortError_stencil("Error while running fill_capacities kernel");

    init_excess_flow_stencil<<<dimBlock_, dimGrid_>>>(excess_flows_, colors_,
            mean_color_foreground, mean_color_background, width_, height_);

    rc = cudaDeviceSynchronize();
    if (rc)
        _abortError_stencil("Error while running init_excess_flow kernel");
}

void Graph_Stencil::push_relabel() {
    cudaError_t rc = cudaSuccess;

    // Init active ptr with MallocManaged to be accessed in host and device
    int* active;
    cudaMallocManaged(&active, sizeof(int));
    *active = 0;
    any_active_stencil<<<dimBlock_, dimGrid_>>>(excess_flows_, heights_,
            width_, height_, height_max_, active);

    rc = cudaDeviceSynchronize();
    if (rc)
        _abortError_stencil("Error while running any_active kernel");
    int count = 7;

    while (*active && count) {
        count --;
        // Run push kernel
        push_stencil<<<dimBlock_, dimGrid_>>>(excess_flows_, heights_, capacities_,
                width_, height_, height_max_);
        rc = cudaDeviceSynchronize();
        if (rc)
            _abortError_stencil("Error while running push kernel");

        // Run relabel kernel
        relabel_stencil<<<dimBlock_, dimGrid_>>>(excess_flows_, heights_, capacities_,
                width_, height_, height_max_);
        rc = cudaDeviceSynchronize();
        if (rc)
            _abortError_stencil("Error while running relabel kernel");
        *active = 0;
        any_active_stencil<<<dimBlock_, dimGrid_>>>(excess_flows_, heights_, width_,
                height_, height_max_, active);

        rc = cudaDeviceSynchronize();
        if (rc)
            _abortError_stencil("Error while running any_active kernel");
    }
}

void Graph_Stencil::cut(Image &img, size_t* h) {
    std::queue<std::pair<size_t, size_t>> q;
    for (auto i = 0u; i < img.height(); i++)
        for (auto j = 0u; j < img.width(); j++)
        {
            if (img.get_mask(i, j) == 1)
                q.push(std::make_pair(i, j));
        }

    Table<int> marks(img.width(), img.height());
    std::pair<size_t, size_t> cur;
    while (!q.empty())
    {
        cur = q.front();
        q.pop();
        if (marks[cur.first][cur.second] != 0)
            continue;
        vector<Neigh> neighbour = neighbours(cur.first, cur.second);
        img.get_mask(cur.first, cur.second) = 1;
        marks[cur.first][cur.second] = 1;
        for (auto n: neighbour)
        {
                if (h[n.i*img.width()+ n.j] > 0) {
                    if (img.get_mask(n.i, n.j) != 1)
                        q.push(std::make_pair(n.i, n.j));
                }
        }
    }
}

char* Graph_Stencil::graph_cut(Image& img) {
    push_relabel();
    size_t* height_array = (size_t*)heights_.copy_from_device();
    cut(img, height_array);
    return img.get_mask_array();
}

std::vector<Neigh> Graph_Stencil::neighbours(int i, int j) const {
    vector<Neigh> n;
    if (!is_border(i+1, height_))
        n.push_back({i+1, j, 1, 0}); //bottom
    if (!is_border(i-1, height_))
        n.push_back({i-1, j, 0, 1}); //top
    if (!is_border(j-1, width_))
        n.push_back({i, j-1, 2, 3}); //left
    if (!is_border(j+1, width_))
        n.push_back({i, j+1, 3, 2}); //right
    return n;
}

