//
// Created by mattrouss on 5/26/20.
//

#pragma once


#include <cstddef>
#include <cmath>
#include <vector>
#include <algorithm>
#include <iostream>

#include "color.hh"

/**
 * 3D Histogram of pixels in RGB space.
 */
class Histogram {
public:
    Histogram(size_t pixel_groups): pixel_groups_(pixel_groups) {
        if (pixel_groups == 0)
            throw std::invalid_argument("Number of pixel groups cannot be 0");

        // Divide RGB space in pixel groups
        n_bins_ = std::ceil(256.f / (float)pixel_groups);

        // Initialize 3D vector of bins
        occ_.resize(n_bins_ * n_bins_ * n_bins_);
    }

    /**
     * Get 1D array index of color occurrence bin from RGB color.
     *
     * @param c Color in RGB space.
     * @return Index of the bin in the occurrence array.
     */
    size_t color_idx(const Color& c) const;

    /**
     * Get 1D array index of color occurrence bin from 3D indices.
     *
     * @param i Red index.
     * @param j Green index.
     * @param k Blue index.
     *
     * @return Index of the bin in the occurrence array.
     */
    size_t color_idx(size_t i, size_t j, size_t k) const;

    /**
     * Add a color to the histogram.
     *
     * @param c Color to add.
     */
    void add_color(const Color& c);

    /**
     * Get occurrence value for color.
     *
     * @param c Color to get occurrence of.
     * @return Numbre of occurrences in bin.
     */
    size_t get_occ(const Color& c) const;

    /**
     * Get log-likelihood of color according to histogram distribution.
     *
     * @param c Color to get log-likelihood of.
     * @return Log-likelihood of color.
     */
    float get_regional_penalty(const Color& c) const;

    friend std::ostream& operator<<(std::ostream &out, const Histogram& h);

private:
    size_t pixel_groups_, n_bins_, hist_size_ = 0;
    std::vector<size_t> occ_; // Bin occurrence vector.
};

