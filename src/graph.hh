//
// Created by mattrouss on 5/24/20.
//

#pragma once

#include <array>
#include <cmath>
#include <algorithm>

#include "neigh.hh"
#include "table.hh"
#include "image.hh"
#include "neigh.hh"
#include "graph_cut_utils.hh"

using namespace std;

class Graph {
public:
    Graph(const Image& img) {
        width = img.width();
        height = img.height();

        height_max_ = HEIGHT_MAX;

        excess_flow_ = Table<int>(width, height);
        height_ = Table<size_t>(width, height);

        for (auto& cap: capacities_)
            cap = Table<int>(width, height);
    }

    /**
     * Use the push relabel algorithm to perform a graph cut.
     *
     * @return Table<bool> partitioning the image. `True` corresponds to
     * foreground, `False` to background.
     */
    char* graph_cut(Image &img);

private:

    //TODO: fiare la doc
    vector<Neigh> neighbours(int i, int j) const;

    //TODO: faire la doc
    bool any_active() const;

    /**
     * Initialize capacities, heights and excess flow for graph cut algorithm.
     *
     * @param img Image container.
     */
    void preflow(const Image& img);
    void preflow_histogram(const Image &img);

    /**
     * Push the given node in the graph.
     *
     * @param i Line coordinate of pixel in image.
     * @param j Column coordinate of pixel in image.
     */
    void push(int i, int j);

    /**
     * Relabel the given node in the graph.
     *
     * @param i Line coordinate of pixel in image.
     * @param j Column coordinate of pixel in image.
     */
    void relabel(size_t i, size_t j);

    /**
     * Perform the push_relabel algorithm on the graph.
     */
    void push_relabel();

    void cut(Image &img);
    Color compute_mean(const Image &img, int seed);

    void extend(Image &img, size_t i, size_t j);
    void smooth(Image &);
    void fill(Image &img);
    bool elt_struct(char* mask, int idx, int jdx, bool bol);
    char* erosion_dilatation(char* img_mask, bool param);
    size_t height_max_;

    int width;
    int height;
    void height_to_mask(Image &img);
    Table<int> excess_flow_;
    Table<size_t> height_;

    // Residual capacities for neighboring pixels, order is:
    // top, bottom, left, right
    std::array<Table<int>, 4> capacities_;

};



