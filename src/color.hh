#pragma once
#include <ostream>
#include <cstdlib>
#include <cmath>
struct Color
{
    Color(){}
    Color(int r, int g, int b)
    :r(r), g(g), b(b){}

    Color& operator+=(const Color& rhs);
    Color& operator-=(const Color& rhs);
    bool operator==(const Color& rhs) const;
    bool operator!=(const Color& rhs) const;
    bool operator<(const Color& rhs) const;
    Color transfer(size_t num_bins) const;

    int L1_norm() const;
    int L2_norm() const;

    int r, g, b;
};

Color operator+(Color lhs, const Color& rhs);
Color operator-(Color lhs, const Color& rhs);

std::ostream& operator<<(std::ostream &out, Color c);