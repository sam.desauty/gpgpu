#include <sys/types.h>
#include <dirent.h>

#include <CLI/CLI.hpp>

#include "image.hh"
#include "graph.hh"
#include "graph_naive.hh"
#include "graph_stencil.hh"

#include <chrono>

void cut_image(const std::string& input_path, const std::string& output_path, const std::string& mode) {
    size_t extension_index = input_path.find_last_of(".");
    std::string raw_name = input_path.substr(0, extension_index);

    auto mask_path = raw_name + std::string("_a.ppm");
    Image img(input_path, mask_path, 25);

    char* mask = nullptr;

    auto t0 = std::chrono::high_resolution_clock::now();
    if (mode == "CPU") {
        Graph g(img);
        mask = g.graph_cut(img);

    } else if (mode == "GPU"){
        Graph_Naive g(img);
        mask = g.graph_cut(img);
    }
    else {
        Graph_Stencil g(img);
        mask = g.graph_cut(img);
    }
    auto t1 = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0);
    std::cout << "["<< mode << "]" << "Graph cut: " << input_path << " execution time: " << duration.count() << "ms" << std::endl;
    write_mask(mask, img.height(), img.width(), output_path);
}

void check_dir_base_names(const std::string& input_path, const std::string& output_path) {
    // Get input_path base_name
    size_t in_base_name_index = input_path.find_last_of("/");
    std::string in_base_name = input_path.substr(in_base_name_index + 1, 10);
    // Get output_path base_name
    size_t out_base_name_index = output_path.find_last_of("/");
    std::string out_base_name = output_path.substr(out_base_name_index + 1, 10);

    std::size_t found_in_extension = in_base_name.find(".");
    std::size_t found_out_extension = out_base_name.find(".");
    if (found_in_extension != std::string::npos)
        throw std::invalid_argument("File was found in input path. To use graph cut on a single file, please use -f flag.");
    if (found_out_extension != std::string::npos)
        throw std::invalid_argument("File was found in output path. To use graph cut on a single file, please use -f flag.");

}

void cut_directory(std::string& input_path, std::string& output_path, const std::string& mode) {
    check_dir_base_names(input_path, output_path);
    DIR* dirp = opendir(input_path.c_str());
    struct dirent * dp;
    if (mkdir(output_path.c_str(), 0777) == -1)
        std::runtime_error(std::string("Error while creating output path: ") + output_path);

    const std::string output_prefix("output_");

    while ((dp = readdir(dirp)) != NULL) {
        std::string file_name(dp->d_name);

        size_t extension_index = file_name.find_last_of(".");
        std::string extension = file_name.substr(extension_index + 1, 3);

        // If file is not a ppm, skip it
        if (extension != "ppm")
            continue;
        std::string raw_name = file_name.substr(0, extension_index);

        // If file is a mask, skip it
        std::size_t found = raw_name.find("_a");
        if (found != std::string::npos)
            continue;

        // Fix dir paths
        if (input_path[input_path.length() - 1] != '/')
            input_path += std::string("/");
        if (output_path[output_path.length() - 1] != '/')
            output_path += std::string("/");

        cut_image(input_path + file_name, output_path + output_prefix + file_name, mode);
    }
    closedir(dirp);
}

int main(int argc, char* argv[])
{
    bool is_file;
    std::string input_path;
    std::string output_path;
    std::string mode = "CPU";

    CLI::App app{"gpu-graph-cut"};
    app.add_flag("-f,!-d", is_file, "Input file/directory flag")->required();
    app.add_option("-i", input_path, "Input file/dir")->required();
    app.add_option("-o", output_path, "Output file/dir");
    app.add_set("-m", mode, {"GPU", "CPU", "GPU2"}, "Either 'GPU', 'CPU', or 'GPU2'");

    CLI11_PARSE(app, argc, argv);

    if (is_file) {
        if (output_path.empty())
            output_path = "output.ppm";
        cut_image(input_path, output_path, mode);
    } else {
        if (output_path.empty())
            output_path = "output/";
        cut_directory(input_path, output_path, mode);
    }

    return 0;
}
