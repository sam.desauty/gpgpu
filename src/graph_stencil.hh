#pragma once

#include <vector>
#include <queue>

#include "image.hh"
#include "neigh.hh"
#include "table.hh"
#include "graph_cut_utils.hh"
#include "graph_naive.hh"


class Graph_Stencil {
public:
    Graph_Stencil(const Image& img);
    ~Graph_Stencil();

    Color compute_mean(const Image &img, int seed);

    /**
     * Initialize capacities, heights and excess flow for graph cut algorithm on device.
     *
     * @param img Image container.
     */
    void preflow(const Image& img);
    /**
     * Initialize capacities, heights and excess flow for graph cut algorithm on host.
     *
     * Preflow uses the distance of the mean color to get likelihood.
     *
     * @param img Image container.
     */
    void preflow_mean_host(const Image& img);

    /**
     * Initialize capacities, heights and excess flow for graph cut algorithm on host.
     *
     * @param img Image container.
     */
    void preflow_histogram_host(const Image& img);

    /**
     * Perform the push_relabel algorithm on the graph.
     *
     * @param dimBlock Dimension of Device Block
     * @param dimGrid Dimension of Device Grid
     */
    void push_relabel();

    /**
     * Launch the graph cut algorithm to segment the image.
     *
     * @param img Image container.
     * @return char* pointer to mask buffer, allocated with std::malloc.
     */
    char* graph_cut(Image& img);
    void cut(Image &img, size_t* heights);

    std::vector<Neigh> neighbours(int i, int j) const;

private:
    int width_, height_;
    size_t height_max_;

    dim3 dimBlock_, dimGrid_;

    // Colors table, type: Color
    DeviceTable colors_;

    // Mask table, type: char
    DeviceTable mask_;

    // 4 capacity tables, type: int
    DeviceTable* capacities_;

    // Excess Flow table, type: int
    DeviceTable excess_flows_;

    // Heights table, type: size_t
    DeviceTable heights_;
};



