#pragma once
#include <iostream>
#include <fstream>
#include <map>

#include "color.hh"
#include "histogram.hh"

struct ColorCompare
{
   bool operator() (const Color& lhs, const Color& rhs) const
   {
       return lhs.r < rhs.r && lhs.g < rhs.g && lhs.b < rhs.b;
   }
};

void write_mask(char* mask, size_t height, size_t width, const std::string& filename);

class Image
{
    public :
        Image(const std::string& image, const std::string& mask, size_t n_bins);
        ~Image();

        size_t width() const;
        size_t height() const;

        void read_image(const std::string& m);
        void read_mask(const std::string& m);
        void masked_image(const std::string& filename);
        void mask_writer(const std::string& filename);
        void compute_histogram();

        char* get_image_array() const;
        char* get_mask_array() const;

        Color* get_color_array() const;

        Color& get_color(size_t i, size_t j) const;
        char& get_mask(size_t i, size_t j) const;

    Histogram hist_foreground, hist_background;
private:
        friend class Graph;
        Color* _image;
        char* _mask;
        size_t _width;
        size_t _height;
        size_t size_foreground = 0;
        size_t size_background = 0;
};