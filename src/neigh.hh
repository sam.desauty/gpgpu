#pragma once
#include <iostream>

using namespace std;

struct Neigh
{
    int i;
    int j;
    int xy;
    int yx;
};