//
// Created by mattrouss on 5/24/20.
//

#include <queue>
#include "graph.hh"

vector<Neigh> Graph::neighbours(int i, int j) const
{
    vector<Neigh> n;
    if (!is_border(i+1, height))
        n.push_back({i+1, j, 1, 0}); //bottom
    if (!is_border(i-1, height))
        n.push_back({i-1, j, 0, 1}); //top
    if (!is_border(j-1, width))
        n.push_back({i, j-1, 2, 3}); //left
    if (!is_border(j+1, width))
        n.push_back({i, j+1, 3, 2}); //right
    return n;
}

bool Graph::any_active() const
{
    for (int j = 0; j < width; j++)
        for(int i = 0; i < height; i++)
        {
            if (excess_flow_[i][j] > 0 && height_[i][j] < height_max_) //is active
                return true;
        }
    return false;
}

Color Graph::compute_mean(const Image &img, int seed) {
    Color mean_color = Color(0, 0, 0);
    float count = 0;
    for (size_t i = 0; i < img._height; ++i)
    {
        for (size_t j = 0; j < img._width; ++j)
        {
            if ((int)img._mask[i* img._width + j] == seed)
            {
                mean_color += img._image[i* img._width + j];
                count += 1;
            }
        }
    }
    return mean_color.transfer(count);
}


void Graph::preflow_histogram(const Image &img)
{
    for (size_t i = 0; i < img._height; ++i)
    {
        for (size_t j = 0; j <img._width; ++j)
        {
            height_[i][j] = 0;

            vector<Neigh> neighbour = neighbours(i, j);
            for (auto n: neighbour)
                capacities_[n.xy][i][j] = likelihood(img.get_color(i, j), img.get_color(n.i, n.j));
        }
    }

    auto capacities_sum = Table<int>(img._width, img._height);
    for (auto& cap: capacities_)
        capacities_sum += cap;
    int K = 1 + capacities_sum.max();

    for (size_t i = 0; i < img._height; ++i)
    {
        for (size_t j = 0; j <img._width; ++j)
        {
            if (img.get_mask(i, j) == 1) //foreground
                excess_flow_[i][j] = K;
            else if (img.get_mask(i, j) == 0) //background
                excess_flow_[i][j] = - K;
            else {
                auto object_rp = img.hist_foreground.get_regional_penalty(img.get_color(i, j));
                auto background_rp = img.hist_background.get_regional_penalty(img.get_color(i, j));
                excess_flow_[i][j] = LAMBDA * (background_rp - object_rp);
            }
        }
    }
}
void Graph::preflow(const Image &img) {

    Color mean_color_foreground = compute_mean(img, 1);
    Color mean_color_background = compute_mean(img, 0);

    for (size_t i = 0; i < img._height; ++i)
    {
        for (size_t j = 0; j <img._width; ++j)
        {
            height_[i][j] = 0;

            vector<Neigh> neighbour = neighbours(i, j);
            for (auto n: neighbour)
                capacities_[n.xy][i][j] = likelihood(img.get_color(i, j), img.get_color(n.i, n.j));

            auto f = (img.get_color(i, j)- mean_color_foreground).L1_norm();
            auto b = (img.get_color(i, j) - mean_color_background).L1_norm();
            excess_flow_[i][j] = b - f;
        }
    }
}

void Graph::push(int i, int j) {
    int flow = 0;

    for (auto n : neighbours(i, j))
    {
        if (height_[n.i][n.j] == height_[i][j] - 1)
        {
            flow = min(capacities_[n.xy][i][j], excess_flow_[i][j]);
            excess_flow_[i][j] -= flow;
            excess_flow_[n.i][n.j] += flow;
            capacities_[n.xy][i][j] -=flow;
            capacities_[n.yx][n.i][n.j] += flow;
        }
    }
}

void Graph::relabel(size_t i, size_t j) {
    size_t my_height = height_max_;
    for (auto n : neighbours(i, j))
    {
        if (capacities_[n.xy][i][j] > 0)
            my_height = min(my_height, height_[n.i][n.j] + 1);
    }
    height_[i][j] = my_height;
}

void Graph::push_relabel() {
    int i = 0;
    while(any_active())
    {
        if (i%100 == 0)
            cout << " iteration " << i << endl;
        for (int i = 0u; i < height; i++)
            for (int j = 0u; j < width; j++)
            {
                if (excess_flow_[i][j] > 0 && height_[i][j] < height_max_) //is active
                    relabel(i, j);
            }
        for (int i = 0; i < height; i++)
            for (int j = 0; j < width; j++)
            {
                if (excess_flow_[i][j] > 0 && height_[i][j] < height_max_) //is active
                    push(i, j);
            }
        i++;
    }
}

void Graph::extend(Image &img, size_t i, size_t j)
{
    std::pair<size_t, size_t> pair = std::make_pair(i, j);
    vector<std::pair<size_t, size_t>> v;
    v.emplace_back(pair);
    while (!v.empty())
    {
        pair = v.back();
        v.pop_back();
        if(height_[pair.first][pair.second] > 0)
        {
            vector<Neigh> neighbour = neighbours(pair.first, pair.second);
             img.get_mask(pair.first, pair.second) = 1;
            for (auto n: neighbour)
            {
                if (img.get_mask(n.i, n.j) != 1)
                    v.emplace_back(std::make_pair(n.i, n.j));
            }
        }
    }  
}

void Graph::smooth(Image &img)
{
    for (int i = 0; i < height; i++)
        for (int j = 0; j < width; j++)
        {
            if (img.get_mask(i, j) == 1)
                extend(img, i, j);
        }
}

void Graph::fill(Image &img)
{
     for (int i = 0; i < height; i++)
        for (int j = 0; j < width; j++)
        {
            vector<Neigh> neighbour = neighbours(i, j);
            int count = 0;
            for (auto n : neighbour)
            {
                if (img.get_mask(n.i, n.j) != 1)
                    count++;
            }
            if (count <= 1)
                img.get_mask(i, j) = 1;
        }
}



bool Graph::elt_struct(char* mask, int idx, int jdx, bool bol)
{
    for(int i = idx; i < idx + 9; i++)
    {
        for(int j = jdx; j < jdx + 9; j++)
        {
            if (bol) // dilatation
            {
                if (mask[i * width + j] == 1)
                    return bol;
            }
            else // erosion
            {
                if (mask[i * width + j] != 1)
                    return bol;
            }
        }
    }
    return !bol;
}

char *Graph::erosion_dilatation(char *img_mask, bool param)
{
    char *mask = new char[width * height];
    for (int i = 0; i < height - 9; i++)
        for (int j = 0; j < width - 9; j++)
        {
            if (elt_struct(img_mask, i, j, param))
                mask[i * width + j] = 1;
            else
                mask[i * width + j] = 0;
        }
    return mask;
}

void Graph::height_to_mask(Image &img)
{
    for (int i = 0; i < height; i++)
        for (int j = 0; j < width; j++)
           if (height_[i][j] > 0)
                img.get_mask(i, j) = 1;
}

void Graph::cut(Image &img) {
    std::queue<std::pair<size_t, size_t>> q;
    for (auto i = 0u; i < img.height(); i++)
        for (auto j = 0u; j < img.width(); j++)
        {
            if (img.get_mask(i, j) == 1)
                q.push(std::make_pair(i, j));
        }

    Table<int> marks(img.width(), img.height());
    std::pair<size_t, size_t> cur;
    while (!q.empty())
    {
        cur = q.front();
        q.pop();
        if (marks[cur.first][cur.second] != 0)
            continue;
        vector<Neigh> neighbour = neighbours(cur.first, cur.second);
        img.get_mask(cur.first, cur.second) = 1;
        marks[cur.first][cur.second] = 1;
        for (auto n: neighbour)
        {
                if (height_[n.i][n.j] > 0) {
                    if (img.get_mask(n.i, n.j) != 1)
                        q.push(std::make_pair(n.i, n.j));
                }
        }
    }

}


char *Graph::graph_cut(Image &img)
{
    preflow_histogram(img);
    push_relabel();
    cut(img);
    return img.get_mask_array();
}
