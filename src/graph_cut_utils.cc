//
// Created by mattrouss on 6/12/20.
//

#include "graph_cut_utils.hh"

bool is_border(int i, int length)
{
    return i == -1 || i == length;
}

int likelihood(const Color& a, const Color& b)
{
    auto dist = (a - b).L2_norm();
    return exp(-pow(dist, 2)/SIGMA);
}


